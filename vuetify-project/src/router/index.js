import { createRouter, createWebHistory } from 'vue-router'
import axios from 'axios';

import NotFoundPage from '../views/NotFound.vue'
import Dashboard from '../views/Dashboard.vue'
import ReservEncours from '../views/ReservationAVenir.vue'
import ReservAttente from '../views/ReservationEnCours.vue'
import AccClients from '../views/CompteUtilisateur.vue'
import AccEmployees from '../views/CompteEmploye.vue'
import Plats from '../views/Plat.vue'
import Services from '../views/Service.vue'
import Tables from '../views/Table.vue'
import Login from '../views/Login.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      component: Dashboard,
      name: 'dashboard',
    },
    {
      path: '/reservations/attentes',
      component: ReservAttente,
      name: 'ReservAttente',
    },
    {
      path: '/reservations/en-cours',
      component: ReservEncours,
      name: 'ReservEncours',
    },
    {
      path: '/accounts/clients',
      component: AccClients,
      name: 'AccClients',
    },
    {
      path: '/accounts/employees',
      component: AccEmployees,
      name: 'AccEmployees',
    },
    {
      path: '/plats',
      component: Plats,
      name: 'Plats',
    },
    {
      path: '/services',
      component: Services,
      name: 'Services',
    },
    {
      path: '/tables',
      component: Tables,
      name: 'Tables',
    },
    {
      path: '/login',
      component: Login,
      name: 'Login',
    },
    {
      path: '/:pathMatch(.*)*',
      component: NotFoundPage,
      name: 'notFound',
    }
    
  ]
})

router.beforeEach((to, from, next) => {
  if (to.path === '/login') {
    // Si l'utilisateur est déjà sur la page de connexion, continuez la navigation normalement
    next();
  } else if (!localStorage.getItem("Logged") && to.path !== '/login') {
    next('/login'); // Redirigez uniquement si l'utilisateur n'est pas connecté
  } else if (localStorage.getItem("token")) {
    axios.post("http://localhost:3002/auth/verifyToken", {
      token: localStorage.getItem("token")
    })
      .then(() => {
        // Le jeton est valide, autoriser la navigation vers la page demandée
        next(); // Continuez la navigation
      })
      .catch(() => {
        // Le jeton est invalide, rediriger vers la page de connexion
        axios.post("http://localhost:3002/auth/refresh-tokens", {
          refreshToken: localStorage.getItem("refreshToken"),
        })
          .then((res) => {
            localStorage.setItem("token", res.data.accessToken);
            localStorage.setItem("refreshToken", res.data.refreshToken);
            localStorage.setItem("user", JSON.stringify(res.data.user));
            next(); // Continuez la navigation
          })
          .catch(() => {
            // Le jeton est invalide, rediriger vers la page de connexion
            localStorage.removeItem("Logged");
            localStorage.removeItem("token");
            localStorage.removeItem("refreshToken");
            localStorage.removeItem("user");
            next('/login'); // Utilisez next() au lieu de router.push()
          });
      });
  } else {
    // Aucun jeton n'est trouvé, rediriger vers la page de connexion
    next('/login'); // Utilisez next() au lieu de router.push()
  }
});

export default router