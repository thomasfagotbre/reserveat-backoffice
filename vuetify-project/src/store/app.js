// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: () => ({
    Logged: false,
    token: null,
    refreshToken: null,
    user: null,
  }),
  actions: {
    setLogged(value) {
      this.Logged = value
    },
    getLogged() {
      return this.Logged
    },
    setToken(value) {
      this.token = value
    },
    getToken() {
      return this.token
    },
    setRefreshToken(value) {
      this.refreshToken = value
    },
    getRefreshToken() {
      return this.refreshToken
    },
    setUser(value) {
      this.user = value
    },
    getUser() {
      return this.user
    }
  }
})
