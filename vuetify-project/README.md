# Bienvenue sur ReservEat !!

Ce projet a été réalisé par Mathis Brouard, Yohann Costard et Thomas Fagot.

## Scripts disponibles

Dans le répertoire du projet, vous pouvez exécuter les commandes suivantes :

### `npm install puis npm run dev`

Lancez l'application en mode développement.\
Ouvrez [http://localhost:8080](http://localhost:8080) dans votre navigateur pour le visualiser.

La page se rechargera automatiquement lorsque vous apporterez des modifications.

### `npm run build`

Construisez l'application pour la production dans le dossier `build`.